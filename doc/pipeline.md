# GitLab CI/CD Pipeline

## Workflow

**Stages: Make -> Check -> Registry -> `$ARCH`**

The docker registry step depends on the make and check targets to pass before
it will build and push the image to the registry. However, all of the other
steps depend on the registry image. So if ever a change is made to the
Dockerfile, then the pipeline will need to be ran twice. Just click the retry
for the failing steps after and then the failing stages will run again with
the latest image from the registry.

