FROM alpine:edge

RUN apk update && \
    apk add --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing \
            check \
            curl \
            git \
            make \
            shellcheck \
	    qemu-aarch64 \
	    qemu-arm && \
    curl -L https://github.com/proot-me/proot/releases/download/v5.3.0/proot-v5.3.0-x86_64-static -o /usr/local/bin/proot && \
    chmod +x /usr/local/bin/proot

WORKDIR /usr/src/proot-static-multiarch
COPY . /usr/src/proot-static-multiarch

RUN make install

ENTRYPOINT ["/usr/local/bin/proot-static-multiarch"]

